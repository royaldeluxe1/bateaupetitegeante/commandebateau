/*
Commende bateau petit géante
X axe du roulis (gite) babord tribord
Y axe du tangage avant arrière

0  reception Acc
1  send monitor serie
2  direction gite babord tribord
3  vitesse gite babord tribord
4
5
6  led AR
7  led reception Acc
8  direction tangage Avant Arriere
9  vitesse tangage babord tribord
10  PinArretUrgence
11  PinReset
12  PinProg
13
A0
A1
A2  LedBabord
A3  LedTribord
A4  LedAv
A5  LedArr
*/

// déclaration des numéros des pins
const int dirGite = 2;  // direction gite babord tribord
const int vGite = 3;        // vitesse gite babord tribord
const int ledEtat = 6;         // led de visualisation de l'etat du bateau
const int ledSerial = 7;         // led de visualisation de la reception serial
const int dirTangage = 8;  // tangage Avant Arriere
const int vTangage = 9;     // vitesse tangage babord tribord
const int PinArretUrgence = 10;  //violet
const int PinReset = 11;         //jaunes
const int PinProg = 12;         //
const int ConsigneVitesseGite = A0; //
const int LedBabord = A2;   //rouge  X
const int LedTribord = A3;  //vert   X
const int LedAv = A4;    //orange Y
const int LedArr = A5;   //bleu   Ygc


// Variable
boolean ArretBateau = true;
boolean ReceivedAcc = false;
int ledState = LOW;
unsigned long currentMillis = 0;
long LedMillis = 0;
long LedInterval = 1000;
long LastAccReceived = 0;
int ValGite;       // gite 
int ValTangage;    // tangage
byte startSerial = 0;  // octet de debut de syncro de la rseption sériel de l arduino nano
int consigneMaxTangage = 127;
int consigneMinTangage = 121;
int consigneMaxGite = 129;
int consigneMinGite = 121;



  void cdeBateau (int GiteAcc , int TangageAcc)
  {
    
    digitalWrite(vGite, 255); 
    digitalWrite(vTangage, 255);

    if (GiteAcc < consigneMinGite){   //consigne vers tribord
      digitalWrite(LedBabord, HIGH);
      digitalWrite(LedTribord, LOW);
      digitalWrite(dirGite, LOW);     
    }
    if (GiteAcc > consigneMaxGite){   //consigne vers babord
      digitalWrite(LedBabord, LOW);
      digitalWrite(LedTribord, HIGH);
      digitalWrite(dirGite, HIGH);
    }
    if (TangageAcc > consigneMaxTangage){   //consigne vers Arriere
      digitalWrite(LedAv, LOW);
      digitalWrite(LedArr, HIGH);
      digitalWrite(dirTangage, HIGH);
    }
    if (TangageAcc < consigneMinTangage){   //consigne vers Avant
      digitalWrite(LedAv, HIGH);
      digitalWrite(LedArr, LOW);
      digitalWrite(dirTangage, LOW);
    }
  }

//***********************************************************************************************************************
//********************************************* VOID SET UP *************************************************************
//***********************************************************************************************************************
void setup(){
  Serial.begin(9600);
  // déclaration fonction des pins
  pinMode(LedBabord, OUTPUT);   // A2rouge
  pinMode(LedTribord, OUTPUT);  // A3 vert
  pinMode(LedAv, OUTPUT);    // A4 orange
  pinMode(LedArr, OUTPUT);   // A5 bleu
  pinMode(PinArretUrgence, INPUT_PULLUP);  // 10 violet
  pinMode(PinReset, INPUT_PULLUP);  // 11 jaunes
  pinMode(PinProg, INPUT_PULLUP);   // 12 programation
  pinMode(ledEtat, OUTPUT);  // 6 led de visualisation de l'etat du bateau
  pinMode(ledSerial, OUTPUT);  // 7 led de visualisation de la reception serial
  pinMode(vGite, OUTPUT);        // 3 commande gite
  pinMode(vTangage, OUTPUT);     // 9 commande tangage
  pinMode(dirGite, OUTPUT);      // 2 direction gite
  pinMode(dirTangage, OUTPUT);   // 8 direction tangage
  // Calibration Valeur Maxi de Tangage et Gite
  
}

//***********************************************************************************************************************
//********************************************* VOID LOOP ***************************************************************
//***********************************************************************************************************************
void loop() {
   currentMillis = millis();
   
      
  //******************************************************************************
  //******************** condition coup de poing, reset et programmation *********
  //******************************************************************************
  if (digitalRead(PinArretUrgence) == true) {
    ArretBateau = true;
    LedInterval = 1000;
  }
  else if ( (currentMillis-LastAccReceived) > 400 ){
    ReceivedAcc = false;
    ArretBateau = true;
    digitalWrite(ledSerial, LOW);
    LedInterval = 100;
  }
  else if ( ((digitalRead(PinArretUrgence)) == false) && ((digitalRead(PinReset) == false)) && ((digitalRead(PinProg) == true)) ){
    ArretBateau = false;
    digitalWrite(LedBabord, HIGH);
    digitalWrite(LedTribord, LOW);
    digitalWrite(dirGite, HIGH);
    digitalWrite(LedAv, HIGH);
    digitalWrite(LedArr, LOW);
    digitalWrite(dirTangage, LOW);
  }
  
//  // visualisation des valeur reset, arret d'urgence et etat bateau
//  Serial.println("###############################");
//  Serial.println("PinArretUrgence");
//  Serial.println(digitalRead(PinArretUrgence));
//  Serial.println("PinReset");
//  Serial.println(digitalRead(PinReset));
//  Serial.println("ArretBateau");
//  Serial.println(ArretBateau);
  
  //******************************************************************************
  // ***********************  recuperation donnée Acc X et Y  ********************
  //******************************************************************************

  if (Serial.available()>=4) {
    ReceivedAcc = true;
    byte contS (0);
    do{
      // read the most recent byte (which will be from 0 to 255):
      startSerial = Serial.read();
      if(startSerial==255){
        ValGite = Serial.read();
        ValTangage = Serial.read();
        digitalWrite(ledSerial, HIGH);     // led serial ON
        LastAccReceived = currentMillis;
//        // affichage valeurs recues
        Serial.print("  Octet de debut   ");Serial.println(startSerial);
        Serial.print("  ValGite   ");Serial.print(ValGite);
        Serial.print("  ValTangage   ");Serial.println(ValTangage);
      }
      contS ++;
    }while( (startSerial!=255) || (contS>5) );
  }

   //******************************************************************************
   //******************** condition fonctionne bateau *****************************
   //******************************************************************************
  if (ArretBateau == true) {              // Arret d'urgence déclanché ou démarage programe
    digitalWrite(LedBabord, LOW);        //  arret  
    digitalWrite(LedTribord, LOW);       //  de toutes les commandes
    digitalWrite(LedAv, LOW);         //  des électrovannes
    digitalWrite(LedArr, LOW);        //
    digitalWrite(vGite, 0); 
    digitalWrite(vTangage, 0);
    
    if(currentMillis - LedMillis > LedInterval) { // fonction du clignotement
      LedMillis = currentMillis;
      if (ledState == LOW){
        ledState = HIGH;
      }
      else{
        ledState = LOW;
      }
    }
    digitalWrite(ledEtat, ledState);
  }
  else {        //  Bateau en fonctionnement
    cdeBateau (ValGite , ValTangage);
    digitalWrite(ledEtat, HIGH);
  }
   //******************************************************************************
   //************************* Mode Programmation *********************************
   //******************************************************************************
   
   if ( ((digitalRead(PinArretUrgence)) == false) && ((digitalRead(PinReset) == true)) & ((digitalRead(PinProg) == false)) ){
     if (ValGite < consigneMinGite) {
       consigneMinGite = ValGite;
     }
     if (ValGite > consigneMaxGite) {
       consigneMaxGite = ValGite;
     }
     if (ValTangage > consigneMaxTangage){
       consigneMaxTangage = ValTangage;
     }
     if (ValTangage < consigneMinTangage){
       consigneMinTangage = ValTangage;
     }
   }
   
   
  delay (1);
}
